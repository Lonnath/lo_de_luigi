<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="../css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="../css/personalized.css" type="text/css"/>
        <link rel="stylesheet" href="../css/typography.css" type="text/css"/>
        <link rel="stylesheet" href="../css/animated.css" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro|Cairo|Roboto|Exo+2|Orbitron|Philosopher&display=swap" rel="stylesheet">
        <link rel="shorcut icon" href="../img/icon.png"/>

        <title>Estanteria</title>
    </head>
    <body bgcolor="#F2F0EF">
        <!--Barra de menú principal-->
    <nav class="shadow-sm p-3 mb-5 rounded navbar navbar-expand-lg navbar-dark text-title fondo-banner">
        <a class="navbar-brand" href="#"><div class="text-title font-roboto ml-4">Lumier Shop</div></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <div class=""><span class="navbar-toggler-icon"></span></div>
        </button>
        <div class="collapse navbar-collapse content" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="../index.htm">
                        <div class="text-title"><img class="img" src="../img/iconic/png/home-2x.png"></div><span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <div class="text-title navi font-cairo">Estanteria</div>
                    </a>
                </li>
                <li class="nav-item dropdown tamanio-drop">
                    <a class="nav-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="text-title dropdown-toggle navi font-cairo">Cuenta</div>
                    </a>
                    <div class="drop text-center tamanio-drop">
                        <div class="dropdown-menu tamanio-drop" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Perfil</a>
                            <a class="dropdown-item" href="#">Carrito</a>
                            <a class="dropdown-item" href="#">Configuración</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Cerrar Sesión</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!--Contenido-->
    <div class="container font-src-sans-pro">ARTICULOS</div>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control font-exo2" placeholder="Buscar..." aria-label="Recipient's username" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary font-orbitron confirm-icon boton-buscar" type="button" id="button-addon2"></button>
                    </div>
                </div>
            </div>
            <div class="col-1"></div>
            <div class="col-2"><a class="btn btn-info" href="oferta.jsp">OFERTA</a></div>
            <div class="col-2"><a class="btn btn-danger" href="solicitud.jsp">SOLICITUD</a></div>
        </div>

    </div>
    <div class="container fondo-pagina">

        <div class="row">
            <div class="col-3">
                <div class="nav nav-tabs nav-stacked font-philosopher fondo-table">
                    <div class="font-philosopher ml-3 mt-2">Filtros</div>
                    <div class="boton-filtrar mt-1"><button type="submit" class="btn btn-info">Filtrar</button></div>
                </div>
                <br><br>
                <div class="container text-left list-estanteria">
                    <div class="card bg-white"> 
                        <div class="card-body">
                            <div class="row nav nav-tabs nav-stacked">
                                <div class="font-exo2 font-weight-bold">Modo</div>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkOferta" value="oferta">
                                <label class="form-check-label" for="checkOferta">Oferta</label>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkSolicitud" value="solicitud">
                                <label class="form-check-label" for="checkSolicitud">Solicitudes</label>
                            </div>
                        </div>
                    </div>
                    <div class="card bg-white"> 
                        <div class="card-body">
                            <div class="row nav nav-tabs nav-stacked">
                                <div class="font-exo2 font-weight-bold">Genero</div>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkAventura" value="aventuras">
                                <label class="form-check-label" for="checkAventura">Aventuras</label>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkSciFi" value="scifi">
                                <label class="form-check-label" for="checkSciFi">Sci/Fi</label>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkAutoficcion" value="autoficcion">
                                <label class="form-check-label" for="checkAutoficcion">Autoficción</label>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkPoliciaca" value="policiaca">
                                <label class="form-check-label" for="checkPoliciaca">Policiaca</label>
                            </div>
                        </div>
                    </div>
                    <div class="card bg-white"> 
                        <div class="card-body">
                            <div class="row nav nav-tabs nav-stacked">
                                <div class="font-exo2 font-weight-bold">Clasificación</div>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkFilosofia" value="filosofia">
                                <label class="form-check-label" for="checkFilosofia">Filosofia</label>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkLiteratura" value="literatura">
                                <label class="form-check-label" for="checkLiteratura">Lengua/Literatura</label>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkGeografia" value="geografia">
                                <label class="form-check-label" for="checkGeografia">Geografia/Historia</label>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkCiSociales" value="cSociales">
                                <label class="form-check-label" for="checkCiSociales">Ciencias Sociales</label>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkCiPura" value="cPuras">
                                <label class="form-check-label" for="checkCiPura">Ciencias Puras</label>
                            </div>
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="checkCiNaturales" value="cNaturales">
                                <label class="form-check-label" for="checkCiNaturales">Ciencias Naturales</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-1"></div>
            <div class="col-8">
                <table class="table bg-white">
                    <thead>
                        <tr class="font-philosopher">
                            <th scope="col">Libro</th>
                            <th scope="col-3">Titulo</th>
                            <th scope="col-2">Fecha</th>
                            <th scope="col-2">Autor</th>
                            <th scope="col-3">Intercambio</th>
                            <th scope="col-2">Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="tamanio-contenido-libro">
                            <td class="img-libro align-middle"><img src="../img/recurso.jpg" class="img-libro"></td>
                            <td class="align-middle">example</td>
                            <td class="align-middle">00/00/0000</td>
                            <td class="align-middle">example</td>
                            <td class="align-middle">example</td>
                            <td class="align-middle">example</td>
                        </tr>
                        <tr class="tamanio-contenido-libro">
                            <td class="img-libro align-middle"><img src="../img/recurso1.jpg" class="img-libro"></td>
                            <td class="align-middle">example</td>
                            <td class="align-middle">00/00/0000</td>
                            <td class="align-middle">example</td>
                            <td class="align-middle">example</td>
                            <td class="align-middle">example</td>
                        </tr>
                        <tr class="tamanio-contenido-libro">
                            <td class="img-libro align-middle"><img src="../img/recurso2.jpg" class="img-libro"></td>
                            <td class="align-middle">example</td>
                            <td class="align-middle">00/00/0000</td>
                            <td class="align-middle">example</td>
                            <td class="align-middle">example</td>
                            <td class="align-middle">example</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <!-- Footer -->
    <footer class="page-footer font-small teal pt-4 fondo-footer">

        <!-- Footer Text -->
        <div class="container-fluid text-center text-md-left">

            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->
                <div class="col-md-6 mt-md-0 mt-3">

                    <!-- Content -->
                    <h5 class="text-uppercase font-weight-bold nav nav-tabs color-titulo-footer">Lumier Shop</h5>
                    <br>
                    <div class="col-md-8">
                        <div class="row"><a href="#" class="color-letra-footer">Acerca de Lumier Shop</a></div>
                        <div class="row"><a href="#" class="color-letra-footer">Trabaja con nosotros</a></div>
                        <div class="row"><a href="#" class="color-letra-footer">Politicas y privacidad</a></div>
                    </div>
                </div>
                <!-- Grid column -->

                <hr class="clearfix w-100 d-md-none pb-3">

                <!-- Grid column -->
                <div class="col-md-6 mb-md-0 mb-3">

                    <!-- Content -->
                    <h5 class="text-uppercase font-weight-bold nav nav-tabs color-titulo-footer">Soporte Tecnico</h5>
                    <br>
                    <div class="col-md-8">
                        <div class="row"><a href="#" class="color-letra-footer">Devoluciones</a></div>
                        <div class="row"><a href="#" class="color-letra-footer">Reembolsos</a></div>
                        <div class="row"><a href="#" class="color-letra-footer">Ayuda para compras</a></div>
                    </div>


                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Footer Text -->

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">© 2018 Copyright:
            <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
        </div>
        <!-- Copyright -->

    </footer>
    <!-- Footer -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
