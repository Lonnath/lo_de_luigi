<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="../css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="../css/personalized.css" type="text/css"/>
        <link rel="stylesheet" href="../css/typography.css" type="text/css"/>
        <link rel="stylesheet" href="../css/animated.css" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro|Cairo|Roboto|Exo+2|Orbitron|Philosopher&display=swap" rel="stylesheet">
        <link rel="shorcut icon" href="../img/icon.png"/>

        <title>Solicitud</title>
    </head>
    <body bgcolor="#F2F0EF">
        <!--Barra de menú principal-->
    <nav class="shadow-sm p-3 mb-5 rounded navbar navbar-expand-lg navbar-dark text-title fondo-banner">
        <a class="navbar-brand" href="#"><div class="text-title font-roboto ml-4">Lumier Shop</div></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <div class=""><span class="navbar-toggler-icon"></span></div>
        </button>
        <div class="collapse navbar-collapse content" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="../index.htm">
                        <div class="text-title"><img class="img" src="../img/iconic/png/home-2x.png"></div><span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Estanteria.jsp">
                        <div class="text-title navi font-cairo">Estanteria</div>
                    </a>
                </li>
                <li class="nav-item dropdown tamanio-drop">
                    <a class="nav-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="text-title dropdown-toggle navi font-cairo">Cuenta</div>
                    </a>
                    <div class="drop text-center tamanio-drop">
                        <div class="dropdown-menu tamanio-drop" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Perfil</a>
                            <a class="dropdown-item" href="#">Carrito</a>
                            <a class="dropdown-item" href="#">Configuración</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Cerrar Sesión</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!--Contenido-->

    <div class="col-12 ml-1">

        <div class="row">
            <div class="col-11 ml-2 shadow bg-white">
                <div class="row">
                    <div class="shadow-sm p-3 bg-white rounded col-12 mb-3">Solicitud de Libro</div>
                </div>
                
                    <form>
                        <div class="form-group row">
                            <label for="inputTitulo" class="col-sm-2 col-form-label">Titulo:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputTitulo" placeholder="Titulo.">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputAutor" class="col-sm-2 col-form-label">Autor:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputAutor" placeholder="Nombre.">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputFecha" class="col-sm-2 col-form-label">Año de Publicación:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputFecha" placeholder="DD/MM/AA.">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10"></div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary ml-5">Solicitar</button>
                            </div>
                        </div>
                    </form>
                
            </div>
        </div>
    </div>
    <br><br>
    <!-- Footer -->
    <footer class="page-footer font-small teal pt-4 fondo-footer">

        <!-- Footer Text -->
        <div class="container-fluid text-center text-md-left">

            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->
                <div class="col-md-6 mt-md-0 mt-3">

                    <!-- Content -->
                    <h5 class="text-uppercase font-weight-bold nav nav-tabs color-titulo-footer">Lumier Shop</h5>
                    <br>
                    <div class="col-md-8">
                        <div class="row"><a href="#" class="color-letra-footer">Acerca de Lumier Shop</a></div>
                        <div class="row"><a href="#" class="color-letra-footer">Trabaja con nosotros</a></div>
                        <div class="row"><a href="#" class="color-letra-footer">Politicas y privacidad</a></div>
                    </div>
                </div>
                <!-- Grid column -->

                <hr class="clearfix w-100 d-md-none pb-3">

                <!-- Grid column -->
                <div class="col-md-6 mb-md-0 mb-3">

                    <!-- Content -->
                    <h5 class="text-uppercase font-weight-bold nav nav-tabs color-titulo-footer">Soporte Tecnico</h5>
                    <br>
                    <div class="col-md-8">
                        <div class="row"><a href="#" class="color-letra-footer">Devoluciones</a></div>
                        <div class="row"><a href="#" class="color-letra-footer">Reembolsos</a></div>
                        <div class="row"><a href="#" class="color-letra-footer">Ayuda para compras</a></div>
                    </div>


                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Footer Text -->

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">© 2018 Copyright:
            <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>
        </div>
        <!-- Copyright -->

    </footer>
    <!-- Footer -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
