<%-- 
    Document   : busqueda
    Created on : 20-nov-2019, 18:28:34
    Author     : Luis Mejias
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
    <div class="container font-src-sans-pro">ARTICULOS</div>
        <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="input-group mb-3">
                    <input type="text" class="form-control font-exo2" placeholder="Buscar..." aria-label="Recipient's username" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary font-orbitron" type="button" id="button-addon2"><img class="avanzar"src="img/avanzar.png"></button>
                    </div>
                </div>
            </div>
            <div class="col-2"></div>
            <div class="col-2"><button type="button" class="btn btn-info">OFERTA</button></div>
            <div class="col-2"><button type="button" class="btn btn-danger">SOLICITUD</button></div>
        </div>

    </div>
        <div class="container fondo-pagina">

    <div class="row nav nav-tabs nav-stacked">
        <div class="col-2">
            <div class="nav nav-tabs nav-stacked fondo-table font-philosopher text-center">Filtros
            </div>
            <br><br>
            <div class="container text-left">

                <div class="row nav nav-tabs nav-stacked">
                    
                </div><div class="col-2">Localidad</div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Colombia</label>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Venezuela</label>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Ecuador</label>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Brazil</label>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Mexico</label>
                </div>

                <div class="row nav nav-tabs nav-stacked">
                    <div class="col-2">Genero</div>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Aventuras</label>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Sci/Fi</label>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Autoficción</label>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Policiaca</label>
                </div>

                <div class="row nav nav-tabs nav-stacked">
                    <div class="col-2">Clasificación</div>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Filosofia</label>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Lengua/Literatura</label>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Geografia/Historia</label>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Ciencias Sociales</label>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Ciencias Puras</label>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Ciencias Naturales</label>
                </div>
            </div>
        </div>
        <div class="col-2"></div>
        <div class="col-8">
            <table class="table ">
                <thead>
                    <tr class="font-philosopher">
                        <th scope="col">Titulo</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">Autor</th>
                        <th scope="col">Intercambio</th>
                        <th scope="col">Precio</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>example</td>
                        <td>example/example/example</td>
                        <td>example</td>
                        <td>example</td>
                        <td>example</td>
                    </tr>
                    <tr>
                        <td>example</td>
                        <td>example/example/example</td>
                        <td>example</td>
                        <td>example</td>
                        <td>example</td>
                    </tr>
                    <tr>
                        <td>example</td>
                        <td>example/example/example</td>
                        <td>example</td>
                        <td>example</td>
                        <td>example</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
    </body>
</html>
